<?php

add_action( 'wp_enqueue_scripts', 'elite_child_scripts' );

    function elite_child_scripts(){
        
        wp_enqueue_script( 'table-pagination', get_stylesheet_directory_uri() . '/js/table_pagination.js', array(), '1.0.0', true );
        wp_enqueue_script( 'user_table_ajax', get_stylesheet_directory_uri() . '/js/user_table_ajax.js', array(), '1.0.0', true );
        //wp_enqueue_script( 'user_table_ajax', plugins_url() . '/pie-register-parse-ext/includes/user_table_ajax.js',array(),'1.0.0', true );
    }
   
/** 
* Customers Reset Password Validation 
**/
                                        
/** Check If the Password is the same as old one **/

add_action('validate_password_reset', 'is_pass_as_old', 9, 2 );

    function is_pass_as_old($errors, $user){
        
        
        
        $new_pass = $_POST['pass1'];
        //$new_hashed_pass = wp_hash_password($new_pass); // WP hash for new pass
        $user_old_pass = get_user_meta($user->ID, 'conn_old_pass', true);
        
        $pass_matched = false;
        //$conn_user_name = $user->data->user_login;
        $conn_user_pass = $user->data->user_pass;
        
        
        if(!empty($user_old_pass)){
            
            foreach($user_old_pass as $old_pass){
           
               if(wp_check_password( $new_pass, $old_pass )){
                
                $pass_matched = true; // it is true if user used this password
                           
               } 
            }   
            
        }
        
              
        
        if(!($errors->get_error_message()) && $pass_matched == true ){
                
            $login_error =  '<strong>'.ucwords(__("error","piereg")).'</strong>: '.apply_filters("piereg_wrong_answer",__( 'You cannot use this password. Please type new one',"piereg" ));
		    $errors->add( 'password_reset_mismatch',$login_error );
            $_POST['error'] = $errors->get_error_message();
            
        }else{
            
            $user_old_pass[] = $conn_user_pass; 
            
            update_user_meta($user->ID, 'conn_old_pass', $user_old_pass);
            update_user_meta($user->ID, 'conn_last_used_pass', $conn_user_pass);
            update_user_meta($user->ID, 'conn_parse_updated', '0' );
            
        }

    }
    
/** On user's profile updated **/

    
/** Add Challenge Question to the Form **/

add_filter('challenge_question', 'answer_this', 10, 1);

    function answer_this($user){
       $question = get_user_meta( $user->ID, 'pie_dropdown_3',true);
        $form_data ='
        <div class="field">
            <h4>Challenge Question</h4>
		  <label for="challenge_question">'.$question.'</label>
		  <input type="text" name="challenge_question" id="challenge_question"  size="20" value="" autocomplete="off" required />
		</div>';
        
        return $form_data;
        
    }
    
/** Challenge Answer Validation **/
    
add_action('validate_password_reset', 'is_answer_correct', 10, 2 );

    function is_answer_correct($errors, $user){
        
        $challange = strtolower(get_user_meta( $user->ID, 'pie_text_4', true ));
        
        $answer = strtolower($_POST['challenge_question']);
        
        
        if(isset($challange)){         
            
            
            if(!empty($answer) && $answer != $challange){
                
                if(! ($errors->get_error_message())){
                
                    $login_error =  '<strong>'.ucwords(__("error","piereg")).'</strong>: '.apply_filters("piereg_wrong_answer",__( 'Wrong Answer',"piereg" ));
				    $errors->add( 'password_reset_mismatch',$login_error );
                    $_POST['error'] = $errors->get_error_message();
                }
                
                
                
            } elseif(empty($answer)){
                
                if(! ($errors->get_error_message())){
                    
                    $login_error =  '<strong>'.ucwords(__("error","piereg")).'</strong>: '.apply_filters("piereg_wrong_answer",__( 'Please answer the question',"piereg" ));
				    $errors->add( 'password_reset_mismatch',$login_error );
                    $_POST['error'] = $errors->get_error_message();
                }
                
                
            }  
            
        }
    }
    
/** @todo: add users's parse.com log */
/*add_action('admin_menu', 'parse_users_menu');
    
function parse_users_menu(){
    
    add_users_page( 'Parse Users', 'Parse Users', 'manage_options', 'parse-users', 'parse_users');
    
    function parse_users(){
        
        echo '<h2>Parse Users</h2>';
    }
    
}
*/


    


?>
