<?php
/*
Template Name: Connections Dashboard Layout
*/
?>
<?php global $current_user, $conn_user_balance; ?>
<?php get_header(); ?>

    <?php /** @todo: Client's Name, Image, telephone number. All data for dashboard */ ?>
    <div class="page-content">
        <div class="container">
        <?php if($current_user && $current_user -> roles[0] == 'customer'): ?>
            <div class="col-md-3 col-sm-5 ">
                
                <h3 id="conn-clients-name">
                    <?php 
                        echo (  $current_user->first_name != '' ?  
                                $current_user->first_name . ' ' . $current_user->last_name : 
                                $current_user->display_name 
                            ); 
                    ?>
                </h3>
                <?php 
                /** @todo: If client's phone isn't verified show X (xxx) xxx-xxxx */
                $conn_verified = 0; 
                ?>
                <h4 id="conn-clients-phone">
                <?php echo get_user_meta($current_user->ID,'pie_phone_6',true)&&$conn_verified?
                            '<i alt="Phone number verified." title="Phone number verified." class="icon-2x icon-ok">':
                            '<i alt="Phone number NOT verified." title="Phone number NOT verified." class="icon-2x icon-remove">'; ?> </i>
                
                <?php  
                echo get_user_meta($current_user->ID,'pie_phone_6',true)?
                            get_user_meta($current_user->ID,'pie_phone_6',true):
                            '(xxx) xxx-xxxx'; 
                ?>
                </h4>
                <div class="conn-profile-img col-xs-8 col-xs-offset-1 col-sm-10 col-sm-offset-2">
                
                    <?php 
    
                    $profile_image = get_user_meta($current_user->ID,'pie_profile_pic_7',true);
                    
                    echo ( $profile_image != '' ?
                            '<img  src="' . $profile_image . '" />' :
                            get_avatar($current_user->ID)
                    );
                        
                    echo '<a href="' . home_url("/admin/settings/?edit_user=1") . '">Edit Profile</a>';            
                                 
                    ?>
                
                </div>
            
            </div>
            <!--<div class="conn-divider"></div>
            <div id="conn-first-row" class="col-md-9 col-md-offset-0 col-sm-5 col-sm-offset-2"><?php /** @todo: Make tables for this data */ ?>
                 <div id="conn-available-funds" class="col-md-3"><h4>Available Funds:</h4><span>$ <?php echo $conn_user_balance['balance']; ?></span></div>
                 <div class="col-md-3"><h4>Usage:</h4><span>Some data</span></div>
                 <div class="col-md-3"></div>
                 <div class="col-md-3"></div>
            </div> -->
            <div class="conn-divider"></div>
            <div class="col-md-9 col-sm-12">
                <div class="col-md-12 tables-container">
                    <?php include_once 'tpl-clients-tables.php' ?>    
                </div><!-- end of container -->
            </div>
            <?php else: ?>
            <div>
            
            <p>Please <a href="<?php echo esc_url( home_url( '/' ) . 'login-here' ); ?>">Sign In</a> as a client to see your dashboard</p>
            
            </div>
            <?php endif; ?>
        </div>
    
    </div>
        
    
	
    
    <div class="page-content">

	<?php
	global $post;
	$builder_page = (substr(get_post_field('post_content', $post->ID), 0, 3) === '[vc') ? true : false;
	?>
	
	<?php if(!$builder_page){ echo '<div class="container padding-small">'; } ?>
	
	<?php get_template_part( '/templates/page/content-page-body' ); ?>
	
	<?php if(!$builder_page){ echo '</div>'; } ?>

	</div>

<?php get_footer(); ?>