<?php global $current_user; ?>
<div id="header-slogan">

    <i class="icon-2x icon-envelope"></i>
    <a href="<?php echo home_url('/admin') ?> "><?php echo $current_user->user_email ?></a>
    <i class="icon-2x icon-phone-sign"></i>
    <a href="#"><?php echo get_user_meta($current_user->ID,'pie_phone_6',true)?
                            get_user_meta($current_user->ID,'pie_phone_6',true):
                            '(xxx) xxx-xxxx'; ?></a> <?php /** @TODO: make new user_meta fields Telephone number and Verified(bool) */  ?>

</div>