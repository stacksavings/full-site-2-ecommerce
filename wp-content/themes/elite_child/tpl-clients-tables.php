<?php

    global $result_transactions;
    global $result_phoneMinutes;
    global $current_user;
    //$cur_pass = $_POST['parsePass'];
    
    
    //var_dump($cur_pass);
    //var_dump($result_phoneMinutes);
    
    $i = 0;
    $j = 0;
    //var_dump($result_phoneMinutes);
    
    if( is_array($result_transactions) && !empty($result_transactions) ){
        
        foreach($result_transactions as $result_transaction){
            
            //$tr_row[$i] = $result_transaction;
            $tr_date[$i] = $result_transaction->getCreatedAt()->format('Y-m-d H:i:s');
                if(empty($tr_date[$i])){$tr_date[$i] = 'MISSING DATE';}
            $tr_activity[$i] = $result_transaction->get('activity');
                if(empty($tr_activity[$i])){$tr_activity[$i] = 'MISSING ACTIVITY';}
            $tr_amount[$i] = $result_transaction->get('amount');
                if(empty($tr_amount[$i])){$tr_amount[$i] = 'MISSING AMOUNT';}
            $tr_reference[$i] = $result_transaction->get('referenceId');
                if(empty($tr_reference[$i])){$tr_reference[$i] = 'MISSING REFERENCE';}
            $tr_balance[$i] = number_format(floatval($result_transaction->get('balance')), 4);
                //if(empty($tr_balance[$i])){$tr_balance[$i] = 'MISSING BALANCE';}
                     
            $i++;
            
        }
        
    }
    
    if( is_array($result_phoneMinutes) && !empty($result_phoneMinutes) ){
        
        foreach($result_phoneMinutes as $result_phoneMinute){
            
            if($result_phoneMinute->get('event')=='dice'){
                
                $pm_date[$j] = $result_phoneMinute->getCreatedAt()->format('Y-m-d H:i:s');
                    if(empty($pm_date[$j])){$pm_date[$j] = 'MISSING DATE';}
                $pm_callid[$j] = $result_phoneMinute->get('callid');
                    if(empty($pm_callid[$j])){$pm_callid[$j] = 'MISSING CALLID';}
                $pm_debitminute[$j] = $result_phoneMinute->get('debitMinutes');
                    //if(empty($pm_debitminute[$j])){$pm_debitminute[$j] = 'MISSING DEBIT Minutes';}
                $pm_description[$j] = $result_phoneMinute->get('toEndpoint') . '<br />' 
                            . $result_phoneMinute->get('result') . '<br />'
                            . $result_phoneMinute->get('reason');
                $pm_cost[$j] = $result_phoneMinute->get('cost');           
                     
                $j++;
            
            }
            
        }
        
    }
    
?>


<ul class="nav nav-tabs">
                          <li class="active"><a href="#tab_a" data-toggle="tab">Available Funds</a></li>
                          <li><a href="#tab_b" data-toggle="tab">Usage</a></li>
                          <!--<li><a href="#tab_c" data-toggle="tab">Tab C</a></li>
                          <li><a href="#tab_d" data-toggle="tab">Tab D</a></li> -->
                        </ul>
                            <div class="tab-content">
                                    <div class="tab-pane active" id="tab_a">
                                        <div class="conn-divider"></div>
                                        <h4 style="display: inline;">Account balance: $<?php echo $conn_user_balance['balance']; ?></h4>
                                        <div style="float: right; display: inline">                                        
                                            <span>Filter last </span>
                                            <form style="display: inline" name="Trans" method="GET" action="">
                                            <select name="NumOfTrans">
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                                <option value="500">500</option>
                                                <option value="10000">All</option>
                                            </select>
                                            <span>activities</span><span></span>
                                            <input class="btn-xs conn-table" type="submit"/>
                                            </form>
                                            
                                        </div>
                                        <table id="tableFunds" class="table table-striped table-condensed table-responsive">
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Activity</th>
                                                    <th>Amount</th>
                                                    <th>Reference</th>
                                                    <th>Balance</th>
                                                </tr>
                                            </thead>
                                            <tbody id="avFunds">
                                            
                                            <?php 
                                            for($itr=0; $itr<=$i-1; $itr++){ 
                                            
                                            echo "<tr class=\"avFundsInitData\">
                                                    <td>$tr_date[$itr]</td>
                                                    <td>$tr_activity[$itr]</td>
                                                    <td class=\"text-right\">$tr_amount[$itr]</td>
                                                    <td>$tr_reference[$itr]</td>
                                                    <td class=\"currency\">$tr_balance[$itr]</td>
                                                </tr>";
                                            
                                            
                                             } 
                                             
                                             ?>
                                            
                                            </tbody>                                      
                                        </table>

                                        <div class="col-md-12 text-center">
                                            <ul class="pagination pagination-lg pager" id="myPagerAvFunds"></ul>
                                        </div>
                                    </div>
                                    
                                    <div class="tab-pane" id="tab_b">
                                        <div class="conn-divider"></div>
                                        <table class="table table-striped table-condensed table-responsive">
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Called</th>
                                                    <th>Duration</th>
                                                    <th>Description</th>
                                                    <th>Cost</th>
                                                </tr>
                                            </thead>                                       
                                            <tbody id="phoneMinutes">
                                            
                                            <?php for($ipm=0; $ipm<=$j-1; $ipm++){ 
                                            
                                            echo "<tr>
                                                    <td>$pm_date[$ipm]</td>
                                                    <td>$pm_callid[$ipm]</td>
                                                    <td>$pm_debitminute[$ipm]</td>
                                                    <td>$pm_description[$ipm]</td>
                                                    <td>$pm_cost[$ipm]</td>
                                                </tr>";
                                            
                                             } ?>
                                            
                                            </tbody>
                                        </table>
                                        <div class="col-md-12 text-center">
                                            <ul class="pagination pagination-lg pager" id="myPagerPhoneMin"></ul>
                                        </div>
                                    </div>
                                    <!--<div class="tab-pane" id="tab_c">
                                        <h4>Pane C</h4>
                                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames
                                            ac turpis egestas.</p>
                                    </div>
                                    <div class="tab-pane" id="tab_d">
                                        <h4>Pane D</h4>
                                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames
                                            ac turpis egestas.</p> 
                                    </div>-->
                            </div><!-- tab content -->