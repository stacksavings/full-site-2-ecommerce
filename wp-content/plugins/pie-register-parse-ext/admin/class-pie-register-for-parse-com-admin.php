<?php

class Pie_Register_For_Parse_Com_Admin {

	private $version;
    private $options;

	public function __construct( $version ) {
		$this->version = $version;
	}

	public function enqueue_styles() {

		wp_enqueue_style(
			'pie-register-for-parse-com',
			plugin_dir_url( __FILE__ ) . 'css/pie-register-for-parse-com-admin.css',
			array(),
			$this->version,
			FALSE
		);

	}

    public function parse_settings(){
        
        add_options_page( 'Parse Keys', 'Parse.com Keys', 'manage_options', 'parse-keys', array($this, 'parse_options_page'));
        
    }
    
    public function parse_options_page(){
        
        require_once plugin_dir_path(__FILE__) . 'partials/parse-options-page.php';
    }
       
    public function parse_options_init(){
        
        register_setting('parse-keys', 'parse_app_id');
        register_setting('parse-keys', 'parse_api_key');
        register_setting('parse-keys', 'parse_master_key');
        add_settings_section('add-parse-keys', 'Add Parse.com Keys', array($this, 'parse_section_text'), 'parse-keys');
        add_settings_field('parse_app_id', 'Application ID', array($this, 'app_id_field'), 'parse-keys', 'add-parse-keys' );
        add_settings_field('parse_api_key', 'REST API Key', array($this, 'rest_api_key'), 'parse-keys', 'add-parse-keys' );
        add_settings_field('parse_master_key', 'MASTER Key', array($this, 'master_key'), 'parse-keys', 'add-parse-keys' );
        
    }
    
    public function parse_section_text(){
        
        echo '<p>You can update PARSE.COM credentials here.</p>';
    }
    
    public function app_id_field(){
        $this->options = get_option('parse_app_id');
       printf(
            '<input type="password" id="app-id" name="parse_app_id" value="%s" />',
            isset( $this->options ) ? esc_attr( $this->options) : ''
        );
    }
    
    public function rest_api_key(){
         $this->options = get_option('parse_api_key');
       printf(
            '<input type="password" id="parse_api_key" name="parse_api_key" value="%s" />',
            isset( $this->options ) ? esc_attr( $this->options) : ''
        );
    }
    
    public function master_key(){
         $this->options = get_option('parse_master_key');
       printf(
            '<input type="password" id="parse_master_key" name="parse_master_key" value="%s" />',
            isset( $this->options ) ? esc_attr( $this->options) : ''
        );
    }
   
}
