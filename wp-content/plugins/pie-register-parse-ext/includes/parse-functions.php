<?php
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
/** Custom functions used for Parse.com**/

add_action( 'piereg_user_login_before_redirect_hook', 'save_parse_pass', 5);

function save_parse_pass($user) {
    
    //global $wpdb;
    
    $user_has_parse_account = get_user_meta( $user->data->ID, 'parse_has_account', true );
    $conn_enc_key = wp_hash($user->data->user_login);
    $parse_pastring = $_POST['pwd'];
    
    if( current_user_can('customer') && $user_has_parse_account == 0 ){
        
        /** Parse pass encryt and save */
                    
        //encrypt pass
        $conn_enc_pass = conn_encrypt($parse_pastring, $conn_enc_key);
        
        //store pass
        $updated = update_conn_user_pass( $user->data->ID, $conn_enc_pass['iv'], $conn_enc_pass['enc'] );
        
             
     
       /* $enc_pass = get_conn_user_meta($user->data->ID);
        wp_die('testing -> Before redirect <-testing; <br />
        User ID: ' . $user->data->ID .'<br />
        UserName: ' . $user->data->user_login .'<br />
        User current encrypted pass: ' . $enc_pass->enc . '<br />
        User current decrypted pass: ' . conn_decrypt($enc_pass->iv, $conn_enc_key, $enc_pass->enc));
        
        wp_logout();*/
        
    }
}
/** Woocommerce */

/** Only one item in the cart */

add_filter( 'woocommerce_add_cart_item_data', 'woo_custom_add_to_cart' );

function woo_custom_add_to_cart( $cart_item_data ) {

    global $woocommerce;
    $woocommerce->cart->empty_cart();

    // Do nothing with the data and return
    return $cart_item_data;
}


/** PayPal IPN */

/** Check if customer has purchased product */

add_action('woocommerce_thankyou', 'check_ipn_response', 11);

    function check_ipn_response($conn_order_id){
        
        global $wpdb;
        global $current_user;
        global $woocommerce;
        
        $order = new WC_Order($conn_order_id);
        $total = number_format($order -> get_total(), 2, '.', '');
        $ref = $conn_order_id;
        $type = $order->payment_method_title;
        
        $conn_user_pass = get_conn_user_pass($current_user->ID);
        $current_pass = conn_decrypt($conn_user_pass->iv, wp_hash($current_user->user_login), $conn_user_pass->enc);  
            
        ParseClient::initialize( get_option('parse_app_id'), get_option('parse_api_key'), get_option('parse_master_key'), false);
            
        
        
        echo '<a href="' . esc_url(site_url('admin')) . '"> Click here to return to the Dashboard</a><br />';
       /* echo 'TesT!<br />';
        echo $order->payment_method_title . ' <br />';
        echo $conn_order_id . ' <br />';
        echo number_format($order -> get_total(), 2, '.', '') . ' <br />';
        echo $order -> status . ' <br />';*/
         
         if ($order -> status == 'pending' || $order -> status == 'processing'){
            
            try {
                   
                    $user = ParseUser::logIn("$current_user->user_login", "$current_pass");
       
                    /** "AdjustUserAccountFunds" function replaced with "CreditCardRecharge" and works well */
                    $conn_user_transactions = ParseCloud::run("CreditCardRecharge", array("amount" => "$total", "referenceId" => "$ref", "type" => "$type" ));
                     
                    ParseUser::logOut();
                    
                    $order->update_status('completed');
               
                    } catch(ParseException $error) {
                                            
                        $parse_error = $error->getMessage();
                        
                        //var_dump($parse_error);
                        
                          echo '<script>
                                    alert("Internal Error 5000\n Unable to Adjust Balance due to Authentication Error:\n' . $parse_error . '\n Please contact Administrator ");
                                    window.location.assign("'. home_url() .'");
                                    </script>';
                            wp_logout();
                            exit();
               
                }
         }
    } 

             



   

add_action('wp_head', 'parse_user_data', 99);  

function parse_user_data(){
    
    //global $ppl_ipn;
    global $wpdb;
    global $current_user;
    global $conn_user_balance;
    global $result_phoneMinutes;
    global $result_transactions;
    
    //var_dump($ppl_ipn);
    
    $parse_has_account = get_user_meta($current_user->ID,'parse_has_account', true);
    //$old_pass = get_user_meta($current_user->ID, 'conn_last_used_pass', true);
    $parse_pass_updated = get_user_meta($current_user->ID, 'conn_parse_updated', true);
    $conn_user_pass = get_conn_user_pass($current_user->ID);  
    $num_of_rows = '10000';
    
    
    if($conn_user_pass){
        
        $old_pass = conn_decrypt($conn_user_pass->iv_old, wp_hash($current_user->user_login), $conn_user_pass->enc_old);
        $current_pass = conn_decrypt($conn_user_pass->iv, wp_hash($current_user->user_login), $conn_user_pass->enc);
        //$_POST['parsePass'] = $current_pass; //for AJAX
    }
    
    
    
    //var_dump($conn_user_pass);

    
    
    if(is_user_logged_in() && current_user_can('customer')){
        
      
       //check username and password 
         
        ParseClient::initialize( get_option('parse_app_id'), get_option('parse_api_key'), get_option('parse_master_key'), false); 
        
        
        /** Reset Pasword on Parse.com */
        if($parse_has_account == 1){
            
            if($old_pass != $current_pass  && $parse_pass_updated == '0'){
                
                try {
                    
                                                            
                    $user = ParseUser::logIn("$current_user->user_login", "$old_pass");
                    
                    $is_pass_reset = ParseCloud::run("ResetPassword", array("newPassword" => "$current_pass"));
                    
                    ParseUser::logOut();
                    update_user_meta($current_user->ID, 'conn_parse_updated', '1' ); 
                    /** @todo Encrypt and update enc_old pass field for further use */
                }catch(ParseException $error){
                    
                    $parse_error = $error->getMessage();
                    echo '<script>
                            alert("Unable to Reset the Password\n Please contact Administrator\n Internal Error 5000:\n:' . $parse_error . ' ");
                            window.location.assign("'. home_url() .'");
                            </script>';
                        wp_logout();
                        exit();
                    
                }
                    
                
            } 
            
        } /** end of reset pass  */
       
              
                    
        try {
            if (!empty($_GET['NumOfTrans'])){
                
                $num_of_rows = $_GET['NumOfTrans']; // set nuber of rows
            
            }
            
            
             //var_dump($num_of_rows);
            $user = ParseUser::logIn("$current_user->user_login", "$current_pass");
            //$user = ParseUser::logIn("info", "pass1W0rd"); 
            
           
            
            //var_dump( ParseUser::getCurrentUser());
            //var_dump($user);
            //var_dump($current_user->user_login);
            //var_dump($current_user->user_pass);
            
            
            /** @todo "AdjustUserAccountFunds" function replaced with "CreditCardRecharge" and works well */
            //$conn_user_transactions = ParseCloud::run("CreditCardRecharge", array("amount" => "9.99", "referenceId" => "12389712", "type" => "Credit/Debit Purchase" ));
             /** @todo "CheckUserAccountFunds" function works well */
            $conn_user_balance = ParseCloud::run("CheckUserAccountFunds");
            
            $query_transactions = new ParseQuery("Transactions");
            $query_transactions->includeKey("PhoneMinute");
            $query_transactions->limit($num_of_rows); // limit to at most 10 results
            $query_transactions->addDescending("createdAt");
            $result_transactions = $query_transactions->find();
            
            //$date_phoneMinutes = $query_transactions->get("createdAt");
            
            $query_PhoneMinutes = new ParseQuery("PhoneMinutes");
            $query_PhoneMinutes->limit($num_of_rows);
            $query_PhoneMinutes->addDescending("createdAt");
            $result_phoneMinutes = $query_PhoneMinutes->find();
            
            /*$query_ACL = new ParseQuery("ACL");
            //$query_PhoneMinutes->limit(1);
            $result_ACL = $query_ACL->find();*/
            
            
            ParseUser::logOut();
            
            
            //$date = $result_transactions[0]->getCreatedAt()->format('Y-m-d H:i:s');
            //$est = $result_transactions[0]->get('balance');
            
            //var_dump($result_transactions);
            //var_dump($result_phoneMinutes[2]->get('event'));
            //var_dump($result_ACL);
            
            } catch(ParseException $error) {
                                    
                $parse_error = $error->getMessage();
                
                //var_dump($parse_error);
                
                  echo '<script>
                            alert("Internal Error 5000\n Unable to Access Current Balance due to Authentication Error:\n' . $parse_error . '\n Please contact Administrator ");
                            window.location.assign("'. home_url() .'");
                            </script>';
                    wp_logout();
                    exit();
       
        }

    }
    
}
    
/** @todo: add users's parse.com back-end admin page */
add_action('admin_menu', 'parse_users_menu', 20);
    
function parse_users_menu(){
    
    add_users_page( 'Parse Users', 'Parse Users', 'manage_options', 'parse-users', 'parse_users');
    
    function parse_users(){
        
        echo '<h2>Parse Users</h2>';
    }
    
}


/** @todo: set new password for existing user ? */
/*add_action('profile_update', 'update_parse_pass', 11, 2);

function update_parse_pass($user_id, $old_user_data){
    
    var_dump($old_user_data);
}*/

/** 
*Store or update Parse password at the prefix_conn_user table
* 
* @name update_conn_user_pass
* @param WordPress_user_ID
* @param Initialization_Vector_(IV)
* @param Encrypted_password
* @param True_When_store_old_password_fileds
*  
**/
function update_conn_user_pass($conn_id, $conn_iv, $conn_pass, $old = false){

    
    global $wpdb;
    
    $table_name = $wpdb->prefix . "conn_user";
    
    //echo 'Start Updating <br />';
    
    $old_pass = $wpdb->get_row("SELECT * FROM {$table_name} WHERE user_id={$conn_id}");
    //var_dump($old_pass);
    $wpdb->flush();
    
    
   // echo 'Old password encryted: ' . $old_pass->enc . '<br />';
   // echo 'IV: ' . $old_pass->iv . '<br />';

    if($old_pass != null && $old == true ){
        
        $result = $wpdb->update( $table_name, 
            array(
                'iv_old'    => $conn_iv,
                'enc_old'   => $conn_pass
            ),
            array(
                'user_id'   => $conn_id
            ) 
       );           
        
    } elseif( $old_pass == null ){ // Check if current Customer has record in the table
    //echo 'No records found <br />'; 
    
        $query = $wpdb->prepare("
        		INSERT INTO $table_name
        		( user_id, iv, enc )
        		VALUES ( %d, %s, %s )
        	", 
                array(
        		$conn_id, 
        		$conn_iv, 
        		$conn_pass
        	) ); 
        $result = $wpdb->query( $query );
    
        
    } else {
    //echo 'Old pass found <br/>';    
        $result = $wpdb->update( $table_name, 
            array(
                'iv'    => $conn_iv,
                'enc'   => $conn_pass
            ),
            array(
                'user_id'   => $conn_id
            ) 
       ); 
        
    }

return $result;

}

function get_conn_user_pass($conn_id){
    
/**
 * Get Connection user password
 * @name get_conn_user_pass
 * @var $conn_id WordPress user Id
 * @return object
 * */
    
    
    global $wpdb;    
    $table_name = $wpdb->prefix . "conn_user";
    $pass = $wpdb->get_row("SELECT * FROM {$table_name} WHERE user_id={$conn_id}");
    
    return $pass;
    
}


/**
* PHP mcrypt - encryption of data
**/
function conn_encrypt($pass, $secret_key){
    
    $input = $pass;
    
    /* Open the cipher */
    $td = mcrypt_module_open('rijndael-128', '', 'ofb', '');
    
    /* Create the IV and determine the keysize length, use MCRYPT_RAND
     * on Windows instead */
    $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_DEV_URANDOM);
    $ks = mcrypt_enc_get_key_size($td);
    
    /* Create key */
    $key = $secret_key;
    
    /* Intialize encryption */
    mcrypt_generic_init($td, $key, $iv);
    
    /* Encrypt data */
    $encrypted = mcrypt_generic($td, $input);
    
    /* Terminate encryption handler */
    mcrypt_generic_deinit($td);
    

    mcrypt_module_close($td);
    
       
    return array('iv'=>$iv, 'enc'=>$encrypted); 
        
    
}

/**
*PHP mcrypt -  decryption of data
**/
function conn_decrypt($iv, $key, $pass){   
    
    /* Open the cipher */
    $td = mcrypt_module_open('rijndael-128', '', 'ofb', '');
    
    /* Initialize encryption module for decryption */
    mcrypt_generic_init($td, $key, $iv);
    //var_dump($pass);
    /* Decrypt encrypted string */
    $decrypted = mdecrypt_generic($td, $pass);
    
    /* Terminate decryption handle and close module */
    mcrypt_generic_deinit($td);
    mcrypt_module_close($td);

   
   return $decrypted;     
  
    
}



?>