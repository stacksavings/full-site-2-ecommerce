<?php
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
    
    class Parse_Add_User {
        
        
        
        
        private $users;
        
        private $registered;
        
        public function user_before_validation($user_id){
            
            $registered = 0;
            
            add_user_meta( $user_id, 'parse_has_account', $registered);
            
            
        }
        
        public function parse_registration(){
            
            global  $user_ID;        
            
            get_currentuserinfo();
            
            $wp_user = get_user_by('id', $user_ID);
            
            $user_pass_array = array();
            
            $user_has_parse_account = get_user_meta( $user_ID, 'parse_has_account', true );
            //$user_current_encpass = get_user_meta($user_ID, 'conn_current_pass', true);
            $user_current_encpass =  get_conn_user_pass($user_ID);
            $plain_text_pass = conn_decrypt($user_current_encpass->iv, wp_hash($wp_user->data->user_login), $user_current_encpass->enc);
            
            /*var_dump($plain_text_pass);
            var_dump($user_current_encpass);
            var_dump($user_has_parse_account);
            wp_die();
            wp_logout();*/
            
            
             if(is_user_logged_in() && current_user_can('customer')){ 
                
                ParseClient::initialize( get_option('parse_app_id'), get_option('parse_api_key'), get_option('parse_master_key'), false);

                if($user_has_parse_account == '0'){
                    
                    // Signup
                    $user = new ParseUser();
                    $user->setUsername($wp_user->data->user_login);
                    //$user->setPassword($wp_user->data->user_pass);
                    $user->setPassword($plain_text_pass);
                    //var_dump($conn_pass);
                    try {
                        $user->signUp();
                        $registered = 1;
                        $user_pass_array[] = $wp_user->data->user_pass; 
                        update_user_meta( $user_ID, 'parse_has_account', $registered );
                        update_user_meta( $user_ID, 'conn_old_pass', $user_pass_array ); // array of user's old passwords for validation
                        update_conn_user_pass($user_ID,$user_current_encpass->iv, $user_current_encpass->enc, true ); // save plain text encrypted to database fields iv_old and enc_old
                    
                    
                    } catch (ParseException $ex) {
                            echo 'error in: ' . $ex->getMessage() . '<br />';
                            wp_die();
                           /* echo '<script>
                                alert("Internal Error 5000\n Unable to Access Current Balance due to Authentication Error:\n' . $ex->getMessage() . '\n Please contact Administrator ");
                                window.location.assign("'. home_url() .'");
                                </script>';
                            wp_logout();*/
                    }
                    
                }
            
            }
            
        }
        
    }