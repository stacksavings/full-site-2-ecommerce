<?php

class Pie_Register_For_Parse_Com {
    
    protected $parse_user;

	protected $loader;

	protected $plugin_slug;

	protected $version;
    
    protected $add_user;

	public function __construct() {

		$this->plugin_slug = 'pie-reg-for-parse-com';
		$this->version = '0.1.0';

		$this->load_dependencies();
		$this->define_admin_hooks();

	}

	private function load_dependencies() {

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-pie-register-for-parse-com-admin.php';

		require_once plugin_dir_path( __FILE__ ) . 'class-pie-register-for-parse-com-loader.php';
		$this->loader = new Pie_Register_For_Parse_Com_Loader();
        
        require plugin_dir_path( dirname( __FILE__ ) ) . 'assets/parse/autoload.php';
        
        
        require_once plugin_dir_path(__FILE__) . 'class-parse-add-user.php';
        
        require_once plugin_dir_path(__FILE__) . 'parse-functions.php';
        
        //require_once plugin_dir_path(__FILE__) . 'class-parse-user-data.php';
        
       // require_once plugin_dir_path(__FILE__) . 'ajax-user-data.php';
        

	}

	private function define_admin_hooks() {

		$admin = new Pie_Register_For_Parse_Com_Admin( $this->get_version() );
		$this->loader->add_action( 'admin_enqueue_scripts', $admin, 'enqueue_styles' );
        $this->loader->add_action('admin_menu', $admin, 'parse_settings');
        $this->loader->add_action('admin_init', $admin, 'parse_options_init');
        
        $user = new Parse_Add_User();
        $this->loader->add_action('user_register', $user, 'user_before_validation');
        $this->loader->add_action('piereg_user_login_before_redirect_hook', $user, 'parse_registration', 20);
        

	}

	public function run() {
		$this->loader->run();
	}

	public function get_version() {
		return $this->version;
	}

}
