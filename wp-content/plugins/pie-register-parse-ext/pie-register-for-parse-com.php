<?php
/*
 * Plugin Name:       Pie Register extension for Parse
 * Plugin URI:        
 * Description:       Pier Register extension for Parse.com manages users on Parse.com
 * Version:           0.1.0
 * Author:            Cedomir Djosic
 * Author URI:        
 * Text Domain:       pie-register-for-parse
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}


 /**
 * Create table for Connection Users
 * */
global $conn_user_db_version;
$conn_user_version = '1.0';

function conn_user_table() {
   
   global $wpdb;
   global $conn_user_db_version;

   $table_name = $wpdb->prefix . "conn_user";
   $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
      id mediumint(9) NOT NULL AUTO_INCREMENT,
      UNIQUE KEY id (id),
      user_id bigint(20) NOT NULL,
      iv varbinary(128) NOT NULL,
      enc varbinary(128) NOT NULL,
      iv_old varbinary(128),
      enc_old varbinary(128)      
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql ); 
    
    add_option( 'conn_user_version', $conn_user_db_version );
}

register_activation_hook(__FILE__, 'conn_user_table' );

//End of Connection Users Table creation

require_once plugin_dir_path( __FILE__ ) . 'includes/class-pie-register-for-parse-com.php';

function run_prfpc() {

	$spmm = new Pie_Register_For_Parse_Com();
	$spmm->run();

}

run_prfpc();
