<?php

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'connections_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'ceda14');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '?hIXIvJG VtNigETQ&1{cu0J5VMYll}6DK|Hz86  |@2:X^vnH@G$p<xx>?>o,>C');
define('SECURE_AUTH_KEY',  '&QeJx^ESRJ1R:OFlA~l6znYwyI7Qz>Vupu;,AIc;r%9t;ohGwCqmB69db/ksI /%');
define('LOGGED_IN_KEY',    'V2mj^2s}p*PiYAO- +nYKJWMSAG1ey2C+`:TVDQGE m`moH^2J24/Jx%`fZgQ,_ ');
define('NONCE_KEY',        'p{MKyW|q:hl3jO3_+Q^Hf#A[n{)+>9?<j;VZV&z[+*~>EC+)/U:BFX9)B2Y_/sr[');
define('AUTH_SALT',        '_K&Z9D0ys,]4K^$`HWLb/ Z|B0Kd,~5x-X/]ggFDlpX*46*sqGshSa$sU~Y,oThG');
define('SECURE_AUTH_SALT', 'zrT6DfDu.-_KX!Gb/+luVPY2Y$YF-,X}@<F/-mna<qX4gU3?i!)eL7{uEn_DsE<o');
define('LOGGED_IN_SALT',   'YLh|cmxfX|3n:Rx50}B&$[lp|Y4^X+BHH]v5WuwRrPOV2ka)~]3jdqf2!i8~Soo&');
define('NONCE_SALT',       'd_h}4;~w=4u}-vvAiss a~tE5f(f+C0zw>>.gyx7@iH(/ lKr9$)9+:/BnT+XKA4');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
